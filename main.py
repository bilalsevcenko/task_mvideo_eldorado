def is_prime(n):
    for k in range(2, int(n**0.5) + 1):
        if n % k == 0:
            return False
    return True

def square(a):
    return a * 4, a*a, a*2**0.5

def bank(a, years):
    return a * (1.1 ** years)

if __name__ == '__main__':
    print(is_prime(997), is_prime(64))
    print(square(5))
    print(bank(100, 3))